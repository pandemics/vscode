const vscode = require('vscode');
const path = require('path');
const cp = require('child_process');
const fs = require('fs');

/**
 * @param {vscode.ExtensionContext} context
 */

function publish () {

	vscode.workspace.saveAll (true).then (ok => {
		if (! ok) {
			vscode.window.showWarningMessage ('You must save all the source documents before compiling.')
			return
		}

		// get path to current file
		if (! vscode.window.activeTextEditor) {
			vscode.window.showErrorMessage ('No active document to publish.')
			return;
		}
		const source = vscode.window.activeTextEditor.document.uri.fsPath;

		// check that we're working on a markdown file
		if (path.extname (source) !== '.md') {
			vscode.window.showErrorMessage ('Pandemics can only publish markdown files.')
			return;
		}

		// find project root, or fall back to file directory
		let rootPath
		if (vscode.workspace.workspaceFolders.length) {
			rootPath = vscode.workspace.workspaceFolders
			.map(folder => folder.uri.path)
			.filter((fsPath) => source.startsWith(fsPath))[0]
		} else {
			rootPath = path.dirname(source)
		}
		const sourceRel = path.relative(rootPath, source);
		
		vscode.workspace.saveAll()

		// compile
		vscode.window.withProgress (
			{
				location: vscode.ProgressLocation.Notification,
				title: `Compiling ${sourceRel}...`,
				cancellable: false,
			},
			async (progress) => {
				return new Promise ((resolve, reject) => {
					// + call pandemics in separate thread to avoid blocking
					const logfile = path.join (
						path.dirname(source),
						'pandemics',
						'pandemics.log'
					)
					// ensure folder exists
					fs.mkdirSync (path.dirname(logfile), { recursive: true })
					// call pandemics
					 let child = cp.exec (
						`bash -i -c "pandemics publish ${sourceRel} --verbose" 2> ${logfile}`,
						{
						  cwd: rootPath,
						  shell: true,
						  stdio: "ignore"
						}
					  );
					  child.on('close', function (code) {
						if (code) {
							reject();
						  } else {
							fs.unlinkSync(logfile)
							resolve()
						  } 
					  })
			
				})
			}
		)
		.then (() => {
			vscode.window.showInformationMessage ('Published successfully!')
		})
		.catch (() => {
			vscode.window.showErrorMessage ('Publishing failed. See logfile for details.');
		})
	})
}

function activate (context) {
	let disposable = vscode.commands.registerCommand ('pandemics.publish', publish);
}

// This method is called when your extension is deactivated
function deactivate () { }

module.exports = {
	activate,
	deactivate
}
