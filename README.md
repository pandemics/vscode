# Pandemics

Extension for using [Pandemics](https://pandemics.gitlab.io) directly in VS Code or VS Codium.

## Requirements

You will need Latex installed on your computer to publish to pdf (see Pandemics documentation).

## Usage

- Open your markdown document
- `ctrl+cmd+p` to publish with Pandemics
